# Swedish translation for gnome-chat.
# Copyright (C) 2015 gnome-chat's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-chat package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-chat master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"chat&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2015-05-06 18:12+0000\n"
"PO-Revision-Date: 2015-05-06 23:56+0100\n"
"Language-Team: Swedish <gnome-se-list@gnome.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"X-Generator: Poedit 1.7.6\n"

#: ../data/gnome-chat.desktop.in.in.h:1
msgid "Chat"
msgstr "Chatt"

#: ../data/gnome-chat.desktop.in.in.h:2
msgid "Instant messaging client"
msgstr "Snabbmeddelandeklient"

#: ../data/gnome-chat.desktop.in.in.h:3
msgid "Chat;Talk;IM;Message;VOIP;GTalk;Facebook;Jabber;"
msgstr "Chatt;Talk;IM;Snabbmeddelande;Meddelande;VOIP;GTalk;Facebook;Jabber;"

#: ../data/org.gnome.chat.gschema.xml.in.h:1
msgid "Window size"
msgstr "Fönsterstorlek"

#: ../data/org.gnome.chat.gschema.xml.in.h:2
msgid "Window size (width and height)."
msgstr "Fönsterstorlek (bredd och höjd)."

#: ../data/org.gnome.chat.gschema.xml.in.h:3
msgid "Window position"
msgstr "Fönsterposition"

#: ../data/org.gnome.chat.gschema.xml.in.h:4
msgid "Window position (x and y)."
msgstr "Fönsterposition (x och y)."

#: ../data/org.gnome.chat.gschema.xml.in.h:5
msgid "Window maximized"
msgstr "Fönster maximerat"

#: ../data/org.gnome.chat.gschema.xml.in.h:6
msgid "Window maximized state"
msgstr "Maximerat tillstånd för fönster"

#: ../src/chat-app-menu.ui.h:1
msgid "About Chat"
msgstr "Om Chatt"

#: ../src/chat-app-menu.ui.h:2
msgid "Quit"
msgstr "Avsluta"

#: ../src/chat-contacts-list-dialog.ui.h:1
msgid "New Conversation"
msgstr "Ny konversation"

#: ../src/chat-contacts-list-dialog.ui.h:2
msgid "Cancel"
msgstr "Avbryt"

#: ../src/chat-contacts-list-dialog.ui.h:3
msgid "Done"
msgstr "Klar"

#: ../src/chat-contacts-list-dialog.ui.h:4
msgid "Type to search a contact…"
msgstr "Skriv för att söka en kontakt…"

#: ../src/chat-main-toolbar.ui.h:1
msgid "Conversations"
msgstr "Konversationer"

#: ../src/chat-main-window.c:262
msgid "Instant messaging client for GNOME"
msgstr "Snabbmeddelandeklient för GNOME"

#. Translators: Put your names here
#: ../src/chat-main-window.c:272
msgid "translator-credits"
msgstr ""
"Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"\n"
"Skicka synpunkter på översättningen till\n"
"<tp-sv@listor.tp-sv.se>."

#: ../src/empathy-utils.c:36
msgid "Available"
msgstr "Tillgänglig"

#: ../src/empathy-utils.c:38
msgid "Busy"
msgstr "Upptagen"

#: ../src/empathy-utils.c:41
msgid "Away"
msgstr "Frånvarande"

#: ../src/empathy-utils.c:43
msgid "Invisible"
msgstr "Osynlig"

#: ../src/empathy-utils.c:45
msgid "Offline"
msgstr "Frånkopplad"

#. translators: presence type is unknown
#: ../src/empathy-utils.c:48
msgctxt "presence"
msgid "Unknown"
msgstr "Okänd"
